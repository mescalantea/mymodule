<?php

/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShopBundle\Entity\Repository\TabRepository;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Mymodule extends Module implements WidgetInterface
{
    protected $config_form = false;

    public $tabs = array(
        array(
            'name' => 'My Module Admin', // One name for all langs
            'class_name' => 'AdminMyModule',
            'visible' => true,
            'parent_class_name' => 'ShopParameters',
        )
    );

    public function __construct()
    {
        $this->name = 'mymodule';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Michel Escalante';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Udemy Module for Prestashop 1.7');
        $this->description = $this->l('Udemy Course module created as a demo');

        $this->confirmUninstall = $this->l('Are you sure to uninstall');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MYMODULE_LIVE_MODE', false);
        Configuration::updateValue('MYMODULE_NAME', '');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MYMODULE_LIVE_MODE');

        return parent::uninstall();
    }

    // /**
    //  * Load the configuration form
    //  */
    // public function getContent()
    // {
    //     /**
    //      * If values have been submitted in the form, process.
    //      */
    //     if (((bool)Tools::isSubmit('submitMymoduleModule')) == true) {
    //         $this->postProcess();
    //     }

    //     $this->context->smarty->assign('module_dir', $this->_path);

    //     $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

    //     // return $output.$this->renderForm();

    //     // return $this->display(__FILE__, 'configure.tpl', array(
    //     //     'config_1_form_html' => 'test'
    //     // ));
    // }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $msg = "";
        // $config_1 = Configuration::get('MYMODULE_NAME');

        $this->orderData();


        // echo $config_1;
        // return $this->display(__FILE__, 'dashboard.tpl', array(
        //     'config_1' => $config_1
        // ));

        if (((bool)Tools::isSubmit('submitMymoduleModule')) == true) {

            if (Tools::isSubmit('config_1')) {
                $config_1 = Tools::getValue('config_1');
                if (Validate::isName($config_1)) {
                    // Configuration::updateValue('MYMODULE_NAME', $config_1);
                    $this->postProcess();
                    $msg = $this->displayConfirmation("Config value saved!");
                } else {
                    $msg = $this->displayError("Error saving config value");
                }
            }
        }

        return $msg . $this->renderForm();
    }

    public function orderData(){
        // Your order id
        $id_order = 5;

        // Load order object
        $order = new Order((int) $id_order);

        // Validate customer object
        if (Validate::isLoadedObject($order)) {

            // Get Order Payment Method Name
            echo $order->payment;

            // Get Order Date
            echo $order->date_add;

            // Get Order Customer Id
            echo $order->id_customer;

            // Get Order Total Payment
            echo $order->total_paid;

            // Print Order Object
            echo "<pre>";
            print_r($order);
            echo "</pre>";

            // Get customer details from current order
            $customer = new Customer($order->id_customer);

            // Get Customer First Name
            echo $customer->firstname;

            // Get Customer Last Name
            echo $customer->lastname;

            // Get Customer Email
            echo $customer->email;
        }
    }

    /**
     * Create an entry in sidebar menu of backoffice for a controller
     */
    protected function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int) TabRepository:: findOneIdByClassName($parent);
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMymoduleModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    // array(
                    //     'type' => 'switch',
                    //     'label' => $this->l('Live mode'),
                    //     'name' => 'MYMODULE_LIVE_MODE',
                    //     'is_bool' => true,
                    //     'desc' => $this->l('Use this module in live mode'),
                    //     'values' => array(
                    //         array(
                    //             'id' => 'active_on',
                    //             'value' => true,
                    //             'label' => $this->l('Enabled')
                    //         ),
                    //         array(
                    //             'id' => 'active_off',
                    //             'value' => false,
                    //             'label' => $this->l('Disabled')
                    //         )
                    //     ),
                    // ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        // 'prefix' => '<i class="icon icon-envelope"></i>',
                        // 'desc' => $this->l('Enter a valid email address'),
                        'name' => 'config_1',
                        'label' => $this->l('Config 1'),
                        'required' => true,
                    ),
                    // array(
                    //     'type' => 'password',
                    //     'name' => 'MYMODULE_ACCOUNT_PASSWORD',
                    //     'label' => $this->l('Password'),
                    // ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            // 'MYMODULE_LIVE_MODE' => Configuration::get('MYMODULE_LIVE_MODE', true),
            // 'MYMODULE_ACCOUNT_EMAIL' => Configuration::get('MYMODULE_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            // 'MYMODULE_ACCOUNT_PASSWORD' => Configuration::get('MYMODULE_ACCOUNT_PASSWORD', null),
            'config_1' => Configuration::get('config_1')
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');

        return "MYMODULE_NAME = " . Configuration::get('MYMODULE_NAME');;
    }

    public function hookLeftColumn()
    {
        return "Hello from " . $this->name;
    }

    public function renderWidget($hookName, array $configuration)
    {
        return "renderWidget " . $hookName;
    }

    public function getWidgetVariables($hookName, array $configuration)
    {
    }
}
