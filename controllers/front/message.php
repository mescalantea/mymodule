<?php

// <ModuleName><FileName>ModuleFrontController
class MyModuleMessageModuleFrontController extends ModuleFrontController
{


    public function setMedia()
    {
        $this->registerStylesheet('front-controller-mymodule', 'modules/'. $this->module->name .'/views/css/message.css', array(
            'priority' => 0,
        ));
        return parent::setMedia();
    }

    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('module:mymodule/views/templates/front/message.tpl');
    }

    public function postProcess()
    {
        if(Tools::getIsset('mierda')){
            echo 'Esta es la mierda enviada por POST: ' . Tools::getValue('mierda');
        }
        parent::postProcess();
    }
}
