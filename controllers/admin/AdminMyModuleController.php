<?php

use Symfony\Component\HttpFoundation\JsonResponse;

class AdminMyModuleController extends ModuleAdminController
{
   
    // you can use symfony DI to inject services
    public function __construct()
    {
        // $this->display = 'view';
        $this->bootstrap = true;
       
        parent::__construct();

        // $tpl_dir = $this->tpl_folder;
        // var_dump($tpl_dir);
        // die;


    }

    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('admin_controller.tpl');
    }

    // public function demoAction()
    // {
    //     return $this->render('@Modules/your-module/templates/admin/demo.html.twig');
    // }

    public function postProcess()
    {
        return parent::postProcess(); // IF YOU DON'T DO THIS FOR ANY REASON
    }

    public function ajaxProcessGetBar()
    {
        // return 'foo';
        // return new JsonResponse('foo');


        echo json_encode($this->orderData());
        exit;
    }


    public function orderData()
    {
        // Your order id
        $id_order = 5;

        // Load order object
        $order = new Order((int) $id_order);

        // Validate customer object
        if (Validate::isLoadedObject($order)) {

            return $order;
        }
        return null;
    }
}
