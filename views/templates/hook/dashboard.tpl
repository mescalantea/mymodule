<h2>Update configuration</h2>

<div class="panel">
    <ul class="nav nav-tabs" style="border-bottom: 1px solid #ddd;">
        <li class="active"><a data-toggle="tab" href="#home">Config 1</a></li>
        <li><a data-toggle="tab" href="#menu1">Config 2</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="row">
                <h3 class="modal-title col-12">Config 1 fields</h3>
                <form action="" method="post" class="col-md-4">
                    <div class="form-group">
                        <input name="config_1" value="{$config_1}" type="text" class="form-control" placeholder="" autocomplete="false">
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>

        <div id="menu1" class="tab-pane fade">
            <h3 class="modal-title">Config 2 fields</h3>
            <p>Some content in menu 1.</p>
        </div>
    </div>
</div>