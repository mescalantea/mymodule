<button id="ajax-test">Ajax test</button>

<script>
    jQuery(function($) {
        $('#ajax-test').click(function() {
            console.log('click');
            $.ajax({
                type: 'POST',
                cache: false,
                //dataType: 'json',
                url: 'index.php', // fix this
                data: {
                    ajax: true,
                    controller: 'AdminMyModule',
                    action: 'getBar', // prestashop already set camel case before execute method
                    token: token
                },
                success: function(data) {
                    // something magical
                    //console.log('success');
                    console.log(data);
                },
               
            });
        });
    });
</script>